# Sorry, Odro!
Aggregating and visualising water quality data of Odra river.

# Website
The main website of this project is https://sorryodro.pl.

# Structure of the repository
The **frontend** folder keeps the website code. The **backend** contains database structure, scripts downloading & importing data and exporting it, as well as Grafana dashboard JSON file.

# License
MIT license.