#!/bin/bash

# which river data relates to?
RIVER="Odra"

# files and folders
source ../../paths.conf

# database login details
source ../../database.conf

# inform about starting the script
echo "[`date`] Script starts"

# change current dir
cd $IMPORTSCRIPTDIR

# does IMPORTSOURCES file exist?
if [ -f $IMPORTSOURCES ]; then

    # does the directory for downloading data exist?
    if [ -d "$IMPORTDOWNLOADDIR" ]; then
	# yes, remove its contents
        rm $IMPORTDOWNLOADDIR/*
    else
        # no, create it
	mkdir $IMPORTDOWNLOADDIR
    fi

    # inform about finishing the script
    echo "[`date`] Downloading & importing data on stations"

    # iterate over every line in the IMPORTSOURCES file
    sed 1d $IMPORTSOURCES | while IFS=";" read -r STATION OWNER EMAIL URL
    do
	# insert the station name and other details into table stations (if not already present)
	STATION_ABBR=`echo "$URL" | awk -F "data/" '{ print $2; }' | awk -F "." '{ print $1; }'`
	COUNT=`MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="SELECT COUNT(*) AS RECORDNUM FROM stations WHERE station_abbr='$STATION_ABBR';"`
	COUNT=`echo $COUNT | awk -F " " '{ print $2; }'`
	if [[ "$COUNT" -eq 0 ]]; then
	    MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="INSERT INTO stations(station_abbr, station_full, owner, email, url) VALUES ('$STATION_ABBR', '$STATION', '$OWNER', '$EMAIL', '$URL');"
	fi

	# dowloadn the datafile
	wget -q -P $IMPORTDOWNLOADDIR/ $URL
    done

    # does the directory for storing data exist?
    if [ -d "$IMPORTDATADIR" ]; then
	# yes, remove its contents
        rm $IMPORTDATADIR/*
    else
        # no, create it
	mkdir $IMPORTDATADIR
    fi

    # unzip all downloaded files
    for ZIPPEDFILE in $IMPORTDOWNLOADDIR/*; do
	unzip -q $ZIPPEDFILE
    done

    # inform about finishing the script
    echo "[`date`] Downloading & importing data on measurements"

    # for each unzipped file perform a set of operations
    for DATAFILE in $IMPORTDATADIR/*; do
	# get the abbreviated station name
	STATION_ABBR=`echo "$DATAFILE" | awk -F "/" '{ print $2; }' | awk -F "." '{ print $1; }'`

	# remove quotes
	sed -i '' 's/"//g' $DATAFILE

	# subsitute "E" (no data) with NULL
	sed -i '' 's/E/NULL/g' $DATAFILE

	# iterate over every line in the DAT file
	sed 1d $DATAFILE | while IFS="," read -r MEASUREMENT_DATE TEMPERATURE CONDUCTIVITY PH DISSOLVED_OXYGEN
	do
	    COUNT=`MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="SELECT COUNT(*) AS RECORDNUM FROM measurements WHERE river='$RIVER' AND station_abbr='$STATION_ABBR' AND measurement_date='$MEASUREMENT_DATE';"`
	    COUNT=`echo $COUNT | awk -F " " '{ print $2; }'`
	    if [[ "$COUNT" -eq 0 ]]; then
		MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="INSERT INTO measurements(river, station_abbr, measurement_date, temperature, conductivity, pH, dissolved_oxygen) VALUES ('$RIVER', '$STATION_ABBR', '$MEASUREMENT_DATE', $TEMPERATURE, $CONDUCTIVITY, $PH, $DISSOLVED_OXYGEN);"
	    fi
	done
    done

    # remove the files...
    rm $IMPORTDOWNLOADDIR/*
    rm $IMPORTDATADIR/*

    # ...and directories
    rmdir $IMPORTDOWNLOADDIR
    rmdir $IMPORTDATADIR

else
    echo "$IMPORTSOURCES not found!"
fi

# inform about finishing the script
echo "[`date`] Script finishes"
