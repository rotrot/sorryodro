#!/bin/bash
export LANG="en_US.UTF-8"

# files and folders
source ../../paths.conf

# database login details
source ../../database.conf

# Execute the query for 7 days
MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="select * from reliability_7_days;" > $RELIABILITYDIR/$RELIABILITYCSV7

# Execute the query for 30 days
MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="select * from reliability_30_days;" > $RELIABILITYDIR/$RELIABILITYCSV30

# do the files exist?
if [ -f $RELIABILITYDIR/$RELIABILITYCSV7 ] && [ -f $RELIABILITYDIR/$RELIABILITYCSV30 ]; then

    # convert spaces to commas
    sed -i '' 's/\t/,/g' $RELIABILITYDIR/$RELIABILITYCSV7
    sed -i '' 's/\t/,/g' $RELIABILITYDIR/$RELIABILITYCSV30

    # Build and upload the webpage
    cd ../../
    ./build-upload.sh

    # Remove the CSV files
    rm $RELIABILITYDIR/$RELIABILITYCSV7
    rm $RELIABILITYDIR/$RELIABILITYCSV30
fi
