CREATE TABLE `stations` (
  `station_abbr` varchar(30) NOT NULL,
  `station_full` varchar(256) DEFAULT NULL,
  `owner` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`station_abbr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `measurements` (
  `river` varchar(30) NOT NULL,
  `station_abbr` varchar(30) NOT NULL,
  `measurement_date` datetime NOT NULL,
  `temperature` double DEFAULT NULL,
  `conductivity` double DEFAULT NULL,
  `pH` double DEFAULT NULL,
  `dissolved_oxygen` double DEFAULT NULL,
  PRIMARY KEY (`river`,`station_abbr`,`measurement_date`),
  FOREIGN KEY (`station_abbr`) REFERENCES stations(`station_abbr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create or replace view reliability_7_days as select
  CONCAT(UCASE(LEFT(station_abbr, 1)), SUBSTRING(station_abbr, 2)) as "Stacja",
  CONCAT(format(round(((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and temperature is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and dissolved_oxygen is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and conductivity is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and pH is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100)/ 4,4),2), "%") as "Średnia",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and temperature is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Temperatura",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and dissolved_oxygen is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Tlen rozp.",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and conductivity is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Przewodność",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and pH is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '171' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "pH"
from
  stations s
group by
  station_abbr;

create or replace view reliability_30_days as select
  CONCAT(UCASE(LEFT(station_abbr, 1)), SUBSTRING(station_abbr, 2)) as "Stacja",
  CONCAT(format(round(((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and temperature is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and dissolved_oxygen is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and conductivity is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100 +
  (select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and pH is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600)* 100)/ 4,4),2), "%") as "Średnia",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and temperature is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Temperatura",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and dissolved_oxygen is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Tlen rozp.",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and conductivity is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "Przewodność",
  CONCAT(format(round((select count(*) from measurements m where measurement_date >= date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00') and measurement_date <= date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00') and pH is not null and m.station_abbr = s.station_abbr)/(select time_to_sec(timediff(date_format(DATE_SUB(NOW(), INTERVAL '3' HOUR), '%Y-%m-%d %H:00:00'), date_format(DATE_SUB(NOW(), INTERVAL '723' HOUR), '%Y-%m-%d %H:00:00'))) / 3600), 4)* 100, 2), "%") as "pH"
from
  stations s
group by
  station_abbr;
