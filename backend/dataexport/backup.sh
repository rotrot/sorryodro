#!/bin/bash

# files and folders
source ../../paths.conf

# database login details
source ../../database.conf

# inform about starting the script
echo "[`date`] SCRIPT STARTING"

# change current dir
cd $BACKUPDIR

# export data
MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="select * from measurements;" > $BACKUPDIR/$BACKUPCSV

# zip the file
zip -r $BACKUPZIP $BACKUPCSV

# remove the source file
rm $BACKUPDIR/$BACKUPCSV

# inform about finishing the script
echo "[`date`] SCRIPT FINISHES"
