#!/bin/bash

# files and folders
source ../../paths.conf

# database login details
source ../../database.conf

# inform about starting the script
echo "[`date`] SCRIPT STARTING"

# change current dir
cd $EXPORTDIR

# export data
MYSQL_PWD="$DB_PASS" $MYSQL --user="$DB_USER" --host="$DB_HOST" --database="$DB_NAME" --execute="select * from measurements;" > $EXPORTDIR/$EXPORTCSV

# remove the previous zip file
rm $EXPORTDIR/$EXPORTZIP

# zip the file
zip -r $EXPORTZIP $EXPORTCSV

# remove the source file
rm $EXPORTDIR/$EXPORTCSV

# inform about finishing the script
echo "[`date`] SCRIPT FINISHES"
