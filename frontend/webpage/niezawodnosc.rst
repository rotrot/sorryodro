.. title:: Niezawodność stacji

Niezawodność stacji raportujących
=================================

Na niniejszej stronie prezentowane są statystyki jak wygląda niezawodność stacji raportujących w zakresie zbierania danych.

Obliczane jest to jako stosunek liczby próbek danych, które zostały przez stację zebrane, do liczby próbek, które powinny być zebrane - wyrażane jest w procentach. Obliczanie tych statystyk ma miejsce w trybie ciągłym (tj. prezentują zawsze aktualne dane).

Zatem przedstawione tu wartości można interpretować w poniższy sposób:

* 100% - dostarczono wszystkie dane w analizowanym okresie
* 0% - w ogóle nie dostarczono danych w analizowanym okresie

Niezawodność w ostatnich siedmiu dniach
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table::
   :file: ../../backend/reliability/niezawodnosc7dni.csv
   :widths: 20, 16, 16, 16, 16, 16
   :header-rows: 1

Niezawodność w ostatnich trzydziestu dniach
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table::
   :file: ../../backend/reliability/niezawodnosc30dni.csv
   :widths: 20, 16, 16, 16, 16, 16
   :header-rows: 1

.. toctree::
   :maxdepth: 1
   :hidden:

   Strona główna <https://sorryodro.pl>
   Wizualizacja <https://sorryodro.pl/wizualizacja>
   Niezawodność stacji <https://sorryodro.pl/reliability.html>
   Pobierz dane <https://sorryodro.pl/sorryodro-dane.zip>
   Kod źródłowy <https://codeberg.org/rotrot/sorryodro>
   Mastodon <https://mastodon.social/@sorryodro>
   Twitter <https://twitter.com/sorryodro>
