.. title:: O projekcie

O projekcie *Sorry, Odro!*
==========================

Projekt `Sorry, Odro! <https://sorryodro.pl/>`_ pobiera, agreguje i wizualizuje dane pozyskane z automatycznych stacji pomiarowych publikowane w `witrynie GIOŚ <https://pomiary.gios.gov.pl/>`_. Pomiary realizowane są dla dziewięciu stacji, a mierzone są: temperatura, pH, przewodność oraz tlen rozpuszczony.

Wizualizację danych znajdziesz pod adresem `https://sorryodro.pl/wizualizacja <https://sorryodro.pl/wizualizacja>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

W górnym prawym rogu wizualizacji możesz zmienić zakres czasowy. Jeśli zaś chcesz zobaczyć pomiary tylko z wybranych stacji, wystarczy kliknąć je na legendzie (i dodając kolejne przytrzymując przycisk CTRL).

Zasada działania
================

Dane GIOŚ importowane są do bazy danych, a następnie wyświetlane w publicznym panelu na platformie Grafana. Dane źródłowe nie są modyfikowane w żaden sposób. Aby mieć możliwie najświeższe dane, aktualizacja odbywa się co kwardans, przy czym zwróć uwagę, że stacje GIOŚ raportują wartości tylko dla pełnych godzin. Możliwe jest też pobranie danych zagregowanych (szczegóły w kolejnej sekcji).

Pobierz dane
============

Zagregowane dane źródłowe do własnych analiz można pobrać z adresu `https://sorryodro.pl/sorryodro-dane.zip <https://sorryodro.pl/sorryodro-dane.zip>`_.

Kto za tym stoi?
================
Projekt *Sorry, Odro!* jest inicjatywą oddolną, niezależną, niekomercyjną i otwartoźródłową.

Kontakt
=======
* Mastodon `@sorryodro@mastodon.social <https://mastodon.social/@sorryodro>`_
* Twitter: `@sorryodro <https://twitter.com/sorryodro>`_
* Email: kontakt@sorryodro.pl

Kod źródłowy
============

Kod źródłowy projektu (baza danych, platforma Grafana, witryna internetowa, zdjęcia Odry) znajdziesz `na platformie Codeberg <https://codeberg.org/rotrot/sorryodro>`_. Jeśli widzisz jakieś błędy lub chcesz pomóc w rozbudowie projektu, użyj funkcjonalności platformy Codeberg (issues, pull requests). Całość opublikowana jest na licencji MIT.

.. toctree::
   :maxdepth: 1
   :hidden:

   Strona główna <https://sorryodro.pl>
   Wizualizacja <https://sorryodro.pl/wizualizacja>
   Niezawodność stacji <https://sorryodro.pl/niezawodnosc.html>
   Pobierz dane <https://sorryodro.pl/sorryodro-dane.zip>
   Kod źródłowy <https://codeberg.org/rotrot/sorryodro>
   Mastodon <https://mastodon.social/@sorryodro>
   Twitter <https://twitter.com/sorryodro>
